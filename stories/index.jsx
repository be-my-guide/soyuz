import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, button, number, text, withKnobs } from '@storybook/addon-knobs';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import faker from 'faker';
import { withTheme } from './withTheme';

import VkIcon from 'react-icons/lib/fa/vk';

import Logo from '../src/components/Logo/Logo';
import MainMenu from '../src/components/MainMenu';
import globals from '../src/themes/global';
import theme from '../src/themes/default';

import { BlueButton, SmallBlueButton, GreenButton } from '../src/components/Buttons';

import CircleAroundIcon from '../src/components/CircleAroundIcon';

import { InputText, TextArea } from '../src/components/form_elements';

import Quote from '../src/components/Quote';

import Directions from '../src/components/blocks/directions/Directions';
import OfferCard from '../src/components/blocks/directions/OfferCard';
import Filter from '../src/components/blocks/directions/Filter';
import offerImg from '../src/components/blocks/directions/offer1.png';

import Feedback from '../src/components/blocks/feedback/Feedback';
import reviewImg from '../src/components/blocks/feedback/author1.png';

import Application from '../src/components/blocks/application/Application';

import Guide from '../src/components/blocks/guides/Guide';
import Guides from '../src/components/blocks/guides/Guides';
import guideImg  from '../src/components/blocks/guides/guide-photo-1.png';

import Intro from '../src/components/blocks/intro/Intro';

import Frontpage from '../src/pages/frontpage';

globals();

const Block = ({ children, ...rest }) => <div style={{ ...rest }}>{children}</div>;

storiesOf('Logo', module)
  .addDecorator(story => <Block backgroundColor="black">{story()}</Block>)
  .add('default', () => <Logo />);

storiesOf('Buttons', module)
  .addDecorator(withTheme(theme))
  .add('Blue', () => <BlueButton>Blue Button</BlueButton>)
  .add('Small Blue', () => <SmallBlueButton>Small Blue Button</SmallBlueButton>)
  .add('Green', () => <GreenButton>Green Button</GreenButton>)

storiesOf('MainMenu', module)
  .addDecorator(withTheme(theme))
  .add('default', () => <MainMenu />);

storiesOf('Misc', module)
  .addDecorator(withTheme(theme))
  .add('Circle around icon', () => (
    <Block padding="2rem">
      <CircleAroundIcon>
        <VkIcon/>
      </CircleAroundIcon>
    </Block>
  ));


storiesOf('Блок Наши направления', module)
  .addDecorator(withTheme(theme))
  .addDecorator(withKnobs)
  .add('All together', () => <Directions/>)
  .add('Offer card', () => (
    <Row>
      <Col xs={3}>
        <OfferCard
          imgUrl={offerImg}
          price="30 000 руб."
          days="10"
          difficulty="сложно"
          type="без рюкзака"
          km="10"
        />
      </Col>
    </Row>
  ))
  .add('Filter', () => (
    <Filter/>
  ));


storiesOf('Блок Отзывы', module)
  .addDecorator(withTheme(theme))
  .addDecorator(withKnobs)
  .addDecorator(story => <Grid>{story()}</Grid>)
  .add('Block', () => <Feedback/>);

storiesOf('Form Elements', module)
  .addDecorator(withTheme(theme))
  .addDecorator(story => <Block padding="1rem">{story()}</Block>)
  .add('Text Input', () => <InputText placeholder="Text input field" />)
  .add('Textarea', () => <TextArea placeholder="Text input field" />);

storiesOf('Application', module)
  .addDecorator(withTheme(theme))
  .add('Block', () => <Grid><Application/></Grid>);

storiesOf('Блок Наши гиды', module)
  .addDecorator(withTheme(theme))
  .add('Guide', () => (
    <Row>
      <Col xs={3}>
        <Guide
          imgUrl={guideImg}
          name="Пётр Мамонов"
          comment="27 подходов"
        />
      </Col>
    </Row>
  ))
  .add('Block', () => (
    <Block><Guides /></Block>
  ));

storiesOf('Блок Слайдер сверху на главной', module)
  .addDecorator(withTheme(theme))
  .addDecorator(withKnobs)
  .add('Block', () => <Intro />);


storiesOf('Pages', module)
  .addDecorator(withTheme(theme))
  .add('Главная', () => <Frontpage />)

