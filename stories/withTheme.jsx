import * as React from 'react';
import { ThemeProvider } from 'styled-components';

/**
 * Storybook styled-components theme provider
 */

export function withTheme(theme) {

  return function(render, context) {
    return(
      <ThemeProvider theme={ theme }>
        { render() }
      </ThemeProvider>
    );
  };

}
