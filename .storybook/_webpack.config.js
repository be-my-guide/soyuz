const path = require('path');

module.exports = (baseConfig, env, defaultConfig) => {

  baseConfig.module.rules.push({
    test: /\.jsx?$/,
    loader: require.resolve('babel-loader')
  });


  return baseConfig;
};
