import { configure } from '@storybook/react';
import { injectGlobal } from 'styled-components';

function loadStories() {
  require('../stories/index.jsx');
}

configure(loadStories, module);
