const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

exports.default = {

  devtool: 'cheap-module-eval-source-map',

  devServer: {
    contentBase: path.join(__dirname, 'dist/'),
    publicPath: '/',
    stats: 'minimal'
  },

  entry: {
    app: ["./src/index.jsx"]
  },

  resolve: {
    extensions: ['.css', '.js', '.jsx', '.png'],

    modules: [
      "./src",
      "node_modules"
    ],

    plugins: [
      // new TsConfigPathsPlugin(),
    ]
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          {
            loader: 'babel-loader',
          }
        ],
        exclude: /node_modules/
      },

      {
        test: /\.png$/,
        use: 'file-loader'
      },

      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'СОЮЗ',
      // filename: path.join(STATIC_ROOT, 'index.html'),
      template: path.join(__dirname, 'src/index.tmpl.html'),
    }),

    new ExtractTextPlugin("styles.css"),
  ],

  output: {
    path: path.join(__dirname, 'dist/'),
    publicPath: '/',
    filename: '[name].js'
  }


};
