import React from 'react';
import styled from 'styled-components';

export const CarouselLegend = styled.div.attrs({
  className: 'legend'
})`
  align-items: center;
  background-color: ${p => p.bgColor || 'white'} !important;
  border-radius: 0 !important;
  bottom: 40px !important;
  color: black !important;
  display: flex;
  justify-content: center;
  opacity: 1 !important;
  margin-left: -50%;
  text-align: left !important;
  top: 40px !important;
  padding: 0;
  width: 100%;
`;

export default CarouselLegend;
