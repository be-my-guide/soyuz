import React from 'react';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { Col, Grid, Row } from 'react-styled-flexboxgrid';
import Logo from './Logo/Logo';
import { ThinBlueButton } from './Buttons'
import { media } from './utils';


export const MainMenu = () => (

  <Bg>
    <GridStyled>
      <RowStyled middle="xs">
        <Col xs={6} md={2} className="menu__logo">
          <Logo />
        </Col>

        <Col xs={12} md={7} className="menu__links">
          <Links>
            <a href="/">Главная</a>
            <a href="/tours">Туры</a>
            <a href="/blog">Блог</a>
            <a href="/individual_tours">Индивидуальный тур</a>
            <a href="#">Контакты</a>
          </Links>
        </Col>

        <Col xs={4} xsOffset={2} md={2} mdOffset={1} className="menu__contacts">
          <Contacts>
            <Phone>
              <span>+7 (922)&nbsp;</span>
              <span>505-09-91</span>
            </Phone>

            <ThinBlueButton>Заказать звонок</ThinBlueButton>

          </Contacts>
        </Col>

      </RowStyled>
    </GridStyled>
  </Bg>

);

export default MainMenu;

const Bg = styled.div`
  background-color: ${p => transparentize(0.5, p.theme.color.darkblue)};
  height: ${p => p.theme.layout.mainmenuHeight};
  position: relative;
  z-index: 20;

  ${media.md`
    position: fixed;
    width: 100%;
  `}  
`;

const GridStyled = styled(Grid)`
  height: inherit;

  flex-wrap: wrap;

  .menu__logo {  order: 1;  }
  .menu__contacts { order: 2; }
  .menu__links { order: 3; }

  ${media.md`
    .menu__logo {  order: 1;  }
    .menu__contacts { order: 3; }
    .menu__links { order: 2; }
  `}

`;

const RowStyled = styled(Row)`
  height: inherit;
`;

const Links = styled.div`
  display: flex;
  font-size: ${p => p.theme.fontsize.md};
  font-weight: 600;
  flex-wrap: wrap;
  justify-content: space-between;    

  
  & > a {
    color: white;
    text-transform: uppercase;
  
    &.active {
      color: green;
    }
  }
`;

const Contacts = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  text-align: center;

  & > button {
    margin-top: 0.25rem;
  }
`;

const Phone = styled.div`
  color: white;
  font-weight: 600;
  
  & > span:nth-child(1) {
    font-size: 1.2rem;
  }
  
  & > span:nth-child(2) {
    font-size: 1.5rem;
  }

`;
