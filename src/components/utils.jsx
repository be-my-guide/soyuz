import { css } from 'styled-components';

/**
 * color overlay mixin
 */
export const withShade = ({ color }) => css`
  &:before {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${p => color};
    z-index: 10;
  }
`;

export const absolutelyStretched = css`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

const mediaQuery = (...query) => (...rules) => css`
  @media ${css`${query}`} {
    ${css`${rules}`}
  }
`;

export const media = {
  xs: mediaQuery`${p => `only screen and (max-width: ${p.theme.flexboxgrid.breakpoints.xs}em)`}`,
  sm: mediaQuery`${p => `only screen and (min-width: ${p.theme.flexboxgrid.breakpoints.sm}em)`}`,
  md: mediaQuery`${p => `only screen and (min-width: ${p.theme.flexboxgrid.breakpoints.md}em)`}`,
  lg: mediaQuery`${p => `only screen and (min-width: ${p.theme.flexboxgrid.breakpoints.lg}em)`}`,
};

export const transparentGradiendAndShadow = css`
  box-shadow: 0px -4px 6px 0px rgba(0, 0, 0, 0.05);
  background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 49%, rgba(255,255,255,0) 100%);
  background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 49%,rgba(255,255,255,0) 100%);
  background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 49%,rgba(255,255,255,0) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#00ffffff',GradientType=0 );
`;
