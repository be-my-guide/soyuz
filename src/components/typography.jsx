import React from 'react';
import styled, { css } from 'styled-components';

const slideTypography = css`
  color: white;

  a {
    color: white;
  }
`;

/**
 * SLIDER/PAGE HEADERS
 */

export const slideTitleClass = css`
  ${slideTypography}
  font-size: 1.9rem;
  font-weight: bold;
  padding: 2rem 0;  
`;

export const SlideTitle = styled.div`
  ${slideTitleClass}
`;

export const slideDescriptionClass = css`
  ${slideTypography}
  font-size: 1.2rem;
  line-height: 140%;
  padding: 1.5rem 0;
`;

export const SlideDescription = styled.div`
  ${slideDescriptionClass}
`;

/**
 * BLOG
 */

export const blogPostTitleClass = css`
  font-size: 1.25rem;
  font-weight: bold;
`;

export const BlogPostTitle = styled.div`
  ${blogPostTitleClass}
`;


export const blogPostDateClass = css`
  color: #8f8f8f;
  font-size: 1rem;
  font-weight: bold;
`;

export const BlogPostDate = styled.div`
  ${blogPostDateClass}
`;


export const blogPostBodyClass = css`
  font-size: 1rem;

  p {
    padding: 1rem 0;
  }
`;

export const BlogPostBody = styled.div`
  ${blogPostBodyClass}
`;


