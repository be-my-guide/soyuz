import React from 'react';
import styled, { css } from 'styled-components';
import { darken } from 'polished';

export const BaseButton = (bg, fg) => styled.button`
  background-color: ${bg};  
  border-radius: 0;
  border: 0;
  color: ${fg};
  cursor: pointer;
  padding: 0.25rem;
  transition: all ${p => p.theme.transition.default};

  &:hover {
    background-color: ${p => darken(0.15, bg)}
  }   

  ${p => p.simple && css`
    background-color: transparent;
    border: 2px solid ${bg};
    color: ${bg};

    &:hover {
      border-color: ${p => darken(0.15, bg)}
    }      
  `}    
 
`;

/**
 * BLUE
 */

export const BlueButton = BaseButton('rgb(0, 89, 215)', 'white').extend`
  font-weight: bold;
  padding: 1rem 1.5rem;
  font-size: ${p => p.theme.fontsize.md};
`;

export const SmallBlueButton = BlueButton.extend`
  font-size: 0.75rem;
`;

export const ThinBlueButton = BlueButton.extend`
  font-size: 0.75rem;
  padding-top: 0.25rem;
  padding-bottom: 0.25rem;
`;

export const MediumBlueButton = BlueButton.extend`
  font-size: 0.75rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

/**
 * GREEN
 */

export const  GreenButton = BaseButton('rgb(0, 255, 25)', 'black').extend`
  font-size:  ${p => p.theme.fontsize.md};
  font-weight: bold;
  padding: 1rem 1.5rem;
`;
