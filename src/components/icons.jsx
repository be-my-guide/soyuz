import React from 'react';
import VkIcon from 'react-icons/lib/fa/vk';
import InstaIcon from 'react-icons/lib/fa/instagram';
import Circle from './CircleAroundIcon';

export const InstaBlueIcon = (
  <Circle backgroundColor={p => p.theme.color.blue}>
    <InstaIcon/>
  </Circle>
);

export const InstaBlueIconLink = props => (
  <a {...props}>{InstaBlueIcon}</a>
);

export const VkBlueIcon = (
  <Circle backgroundColor={p => p.theme.color.blue}>
    <VkIcon/>
  </Circle>
);

export const VkBlueIconLink = props => (
  <a {...props}>{VkBlueIcon}</a>
);



