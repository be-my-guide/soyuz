import React from 'react';
import styled, { css } from 'styled-components';

const center = css`
  ${p => p.center && `
    display: block;
    text-align: center;
  `};
`;

export const BlockTitle = styled.h1`
  display: inline;
  font-size: 1.8rem;
  font-weight: 600;
  text-transform: uppercase;
  ${p => p.color && `
    color: ${p.color};
  `}
  
  ${center};
`;

export const BlockSubtitle = styled.h1`
  display: inline;
  font-size: 1.1rem;
  font-weight: 400;
  margin: 0.25rem 0;
  ${p => p.color && `
    color: ${p.color};
  `}
  
  ${center};
`;

export default BlockTitle;
