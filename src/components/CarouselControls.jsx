import React from 'react';
import { Grid, Row, Col } from 'react-styled-flexboxgrid';
import styled from 'styled-components';
import { transparentize } from 'polished';
import LeftIcon from 'react-icons/lib/fa/angle-left';
import RightIcon from 'react-icons/lib/fa/angle-right';
import Circle from './CircleAroundIcon';

export const CarouselControls = () => {

  const iconStyle = {
    width: '2rem',
    height: '2rem',
    color: transparentize(0.25, 'white')
  };

  const circleProps = {
    size: '3rem',
    backgroundColor: p => transparentize(0.9, 'white'),
    style: {
      cursor: 'pointer',
      margin: '0.5rem',
      zIndex: 10
    },
  };

  return (
    <GridStyled>
      <RowStyled middle="xs">
        <Col xs={1} style={{ zIndex: 10 }}>
          <Circle {...circleProps}><LeftIcon {...iconStyle}/></Circle>
        </Col>
        <Col xs={10} />
        <Col xs={1} style={{ textAlign: 'right', zIndex: '10'}}>
          <Circle {...circleProps}><RightIcon {...iconStyle}/></Circle>
        </Col>
      </RowStyled>
    </GridStyled>
  );
};

export default CarouselControls;

const GridStyled = styled(Grid)`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

const RowStyled = styled(Row)`
  justify-self: stretch;
  flex: 1 0 100%;
`;

