import React from 'react';
import styled from 'styled-components';
import P from 'prop-types';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';

import { GreenButton } from '../../Buttons';
import BlockTitle from '../../typography/BlockTitle';
import { InputText, TextArea } from '../../form_elements';

import bg from './bg.png';
import rucksack from './rucksack.png';
import { absolutelyStretched, transparentGradiendAndShadow } from '../../utils';

export const Application = ({ transpBg, rucksack, withBg }) => (

  <Bg bg={withBg ? bg : null}>
    <Wrapper>

      <ExtraBgStyled  transpBg={transpBg} />

      <GridStyled>
        <Row>

          <Col xs={2} />

          <MainCol xs={8}>

            <BlockTitle center>ДОБРЫЙ ДЕНЬ, ДОРОГОЙ ДРУГ!</BlockTitle>

            <Desc>
              Если у Вас появилось желание путешествовать индивидуально или в кругу своих близких,
              а не в составе большой группы, то мы с удовольствием поможем Вам в организации Вашего отдыха,
              для этого необходимо заполнить данную таблицу.
            </Desc>

            <form>

              <InputText placeholder="Как к вам обращаться?"/>
              <InputText placeholder="Страна посещения"/>
              <TextArea rows="3" placeholder="Маршрут, напишите название, или места, которые вы хотели бы посетить"/>
              <InputText placeholder="Количество человек"/>
              <InputText placeholder="Бюджет на поездку"/>
              <TextArea rows="3" placeholder="Ваши пожелания, укажите как вы хотите передвигаться, уровень комфорта жилья и другие предпочтения"/>
              <InputText placeholder="Способ связи для ответов"/>

              <GreenButton type="submit">ОТПРАВИТЬ ЗАЯВКУ</GreenButton>

              {rucksack && (
                <Rucksack><img src={rucksack} /></Rucksack>
              )}

            </form>


          </MainCol>

          <Col xs={2} />

        </Row>



      </GridStyled>
    </Wrapper>
  </Bg>
);

Application.propTypes = {
  // displays default background
  withBg: P.bool,

  // transp gradient bg  and shadow on edges
  transpBg: P.bool,

  // show rucksack
  rucksack: P.bool
}

Application.defaultProps = {
  rucksack: true
}

export default Application;

const Bg = styled.div`
  ${p => p.bg && `
    background: url(${p => p.bg}) center center;  
  `}
  position: relative;
`;

const ExtraBg = ({ className }) => (
  <div className={className}>
    <Grid>
      <Row>
        <Col xs={1}/>
        <Col xs={10} className="extrabg__center" />
        <Col xs={1}/>
      </Row>
    </Grid>
  </div>
);

const ExtraBgStyled = styled(ExtraBg)`
  ${absolutelyStretched};

  margin: 4rem 0;
  z-index: 2;

  .extrabg__center { background-color: white; }

  ${p => p.transpBg && `
    .extrabg__center {
      ${transparentGradiendAndShadow};
    }
  `}    
  
  div {
    height: 100%;
  }
`;

const Wrapper = styled(Grid)`
  padding: 5rem 0 5rem 0;
`;

const GridStyled = styled(Grid)``;

const MainCol = styled(Col)`
  z-index: 3;

  display: flex;
  flex-direction: column;
  padding-top: 1rem;
  
  & > * {
    margin-bottom: 2rem;
  }
  
  form {
    position: relative;

    input[type=text], textarea {
      width: 100%;
      
      &:not(:last-child) {
        margin-bottom: 0.5rem;
      }
    }
    
    input[type="submit"], button {
      display: block;
      margin: 3rem auto 0 auto;
    }
  }
`;

const Desc = styled.div`
  font-size: 1.25rem;
`;

const Rucksack = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 4;
  
  img {
    margin-left: 90%;
    margin-bottom: -45%;
  }
`;
