import React from 'react';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import label from './taganai.png';
import bg from './taganai_bg.png';

export const slide1 = (

  <Slide bgUrl={bg}>
    <div  style={{ marginTop: '310px', width: '600px' }}>

      <div className="title">8&ndash;12 марта</div>
      <div><img src={label} width="562" /></div>
      <div className="description">
        Этот поход отлично подойдёт для тех, кто не имеет походного опыта и желает
        впервые отправиться в увлекательное путешествие.
      </div>

      <GreenButton style={{ marginTop: '5.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>

);

export default slide1;
