import React from 'react';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import title from './crimea.png';
import bg from './crimea_bg.png';

const slide = (
  <Slide bgUrl={bg}>
    <div style={{ marginTop: '310px', width: '998px', textAlign: 'left' }}>

      <div className="title">ПОХОД ПО КРЫМУ</div>
      <div><img src={title} width="998" height="78" /></div>
      <div className="description">
      В этом походе вы увидите настоящий Крым! То, ради чего сюда едут большинство настоящих туристов - это каньоны, дикие пещеры, водопады, буковые леса и многое другое. Этот маршрут начнётся в Севастополе, 
      а закончится в Ялте.
    </div>

      <GreenButton style={{ marginTop: '4.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>
)

export default slide;
