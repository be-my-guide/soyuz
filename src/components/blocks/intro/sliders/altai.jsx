import React from 'react';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import title from './altai.png';
import bg from './altai_bg.png';

const slide = (
  <Slide bgUrl={bg}>
    <div style={{ marginTop: '310px', width: '974px', textAlign: 'center' }}>

      <div className="title">ПЕШИЙ ПОХОД</div>
      <div><img src={title} width="974" /></div>
      <div className="description">
      Этот поход в самое сердце Алтая, к подножию величественной Белухи, которая уже давно
      стала настоящим символом этих мест и является и является заветной целью практически
      каждого туриста.
    </div>

      <GreenButton style={{ marginTop: '4.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>
)

export default slide;
