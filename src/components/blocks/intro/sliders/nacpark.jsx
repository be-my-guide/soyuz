import React from 'react';
import styled from 'styled-components';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import title from './nacpark.png';
import bg from './nacpark_bg.png';

const Head = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: space-between;
`;

const slide = (
  <Slide bgUrl={bg}>
    <div style={{ marginTop: '310px', width: '1068px', textAlign: 'center' }}>

      <Head>
        <div>
          <div className="title">ЗИМНИЙ ТУР В НАЦИОНАЛЬНЫЙ ПАРК</div>
        </div>
        
        <div>
          <div className="title" style={{ textAlign: 'right' }}>
            3-9 января<br />
            8-13 марта<br />
            23-28 февраля            
          </div>
        </div>
      </Head>      
      
      <div><img src={title} width="1068" height="62" /></div>

      <div className="description">
        Один из самых доступных национальных парков в России, до него близко ехать,
        его тропы не сложны, но он невероятно красив! Зимний поход с комфортом - это Таганай. 
      </div>

      <GreenButton style={{ marginTop: '4.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>
)

export default slide;

