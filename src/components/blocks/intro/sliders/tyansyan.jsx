import React from 'react';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import title from './tyansyan.png';
import bg from './tyansyan_bg.png';

const slide = (
  <Slide bgUrl={bg}>
    <div style={{ marginTop: '310px', width: '974px', textAlign: 'center' }}>

      <div className="title">ПОХОД ПО КИРГИЗИИ</div>
      <div><img src={title} width="775" height="79" /></div>
      <div className="description">
      Не влюбиться в эти места просто невозможно, ведь Тянь-Шань это настоящий рай для туриста. Красивейшая природа, горные вершины, выскогорные озёра, бурные реки и шумные всё это будет на нашем маршруте, ну и конечно же отдых на озере Иссык-Куль.
    </div>

      <GreenButton style={{ marginTop: '4.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>
)

export default slide;
