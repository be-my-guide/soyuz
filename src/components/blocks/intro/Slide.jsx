import React from 'react';
import styled from 'styled-components';
import P from 'prop-types';
import { Grid, Row, Col } from 'react-styled-flexboxgrid';

export const Slide = ({ bgUrl, children }) => (
  <Wrapper src={bgUrl}>
    <Grid>
      <Row>
        <ColStyled xs={10} xsOffset={1}>
          {children}
        </ColStyled>
      </Row>
    </Grid>
  </Wrapper>
);

Slide.propTypes = {
  bgUrl: P.string
}

export default Slide;

const Wrapper = styled.div`
  background: url(${p => p.src}) center center no-repeat;
  background-size: cover;
  height: 860px;

  .title {
    color: white;
    font-size: 1.9rem;
    font-weight: bold;
    padding: 2rem 0;
  }

  .description {
    color: white;
    font-size: 1.2rem;
    line-height: 140%;
    padding: 1.5rem 0;
  }

`;

const ColStyled = styled(Col)`
  text-align: left;
`;
