import React from 'react';
import styled from 'styled-components';
import { Carousel } from 'react-responsive-carousel';
import Controls from '../../CarouselControls';

import altai from './sliders/altai.jsx';
import taganai from './sliders/taganai';
import nacpark from './sliders/nacpark';
import ural from './sliders/ural';
import crimea from './sliders/crimea';
import tyansyan from './sliders/tyansyan';

export const Intro = ({ className }) => (


  <Wrapper className={className}>

    <Carousel
      autoPlay={false}
      showArrows={false}
      showStatus={false}
      showThumbs={false}
    >
      {taganai}
      {altai}
      {nacpark}
      {ural}
      {crimea}
      {tyansyan}
    </Carousel>

    <Controls/>
  </Wrapper>

);

export default Intro;

const Wrapper = styled.div`
  position: relative;
  
  .control-dots {
    z-index: 2; // carousel dots above black overlay
  }
`;
