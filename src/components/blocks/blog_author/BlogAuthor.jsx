import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import Quote from '../../Quote';
import { BlueButton } from '../../Buttons';
import { transparentGradiendAndShadow } from '../../utils';

import pic from './author-photo';

const text = `
Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.
Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение
шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при
простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы
электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию,
так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё
ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много
версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические
варианты).
`;

export const BlogAuthor = () => (
  <Wrapper>
    <GridStyled>
      <Row>
        <ColStyled xsOffset={1} xs={10}>
          <Quote
            fitted
            imgUrl={pic}
            name="Виталий Казаков"
            occupation="директор блога"
            text={text}
            vk="#"
            insta="#"
            extra={<div><BlueButton>Перейти в блог</BlueButton></div>}
          />
        </ColStyled>
      </Row>
    </GridStyled>
  </Wrapper>
);

export default BlogAuthor;

const Wrapper = styled.div`
`;

const GridStyled = styled(Grid)`
  ${transparentGradiendAndShadow};
`;

const ColStyled = styled(Col)`
  padding: 3rem 0;
`;
