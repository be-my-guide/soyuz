import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';

export const OfferCard = props => {

  return (
    <Wrapper>
      <Image url={props.imgUrl} />

      <Price>{props.price}</Price>

      <Features>
        <Feature>Сложность: {props.difficulty}</Feature>
        <Feature>Дни: {props.days}</Feature>
        <Feature>Вид тура: {props.type}</Feature>
        <Feature>Километры: {props.km}</Feature>
      </Features>

    </Wrapper>

  );

};

OfferCard.propTypes = {
  imgUrl: P.string.isRequired,
  price: P.string.isRequired,
  difficulty: P.string.isRequired,
  days: P.string.isRequired,
  type: P.string.isRequired,
  km: P.string.isRequired
};

export default OfferCard;

const Image = styled.div`
  background: url(${p => p.url}) no-repeat ;
  background-size: contain;
  position: relative;
  padding-top: 100%;
`;

const Wrapper = styled.div`

  &:not(:hover) {
    & > ${Image}:before {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: ${p => transparentize(0.79, p.theme.color.blue)}; 
      z-index: 2;
      display: block;
      content: '';
    }
  };

  box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.19);
  cursor: pointer;
  display: flex;
  flex-direction: column;
`;


const Price = styled.div`
  background-color: white;
  text-align: center;
  font-size: 1.25rem;
  font-weight: 600;
  padding: 1rem 0 1rem 0;
`;

const Features = styled.div`
  background-color: #f9f9f9;
  padding: 1rem 1.5rem;
`;

const Feature = styled.div`
  font-size: ${p => p.theme.fontsize.md};
  font-style: italic;
  padding: 0.25rem;
`;
