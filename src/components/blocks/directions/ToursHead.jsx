import React from 'react';
import styled from 'styled-components';
import { SlideDescription } from '../../typography';

import bg from './tours_bg';
import label from './tours_title';

export const ToursHead = () => (
    <Wrapper>
      <Content>
        <SlideDescription>
          Мы подберём тур даже для самого взыскательного туриста, а если вы не нашли подходящего для вас предложения,
          то мы  можем подготовить для вас <a href="#">индивидуальный тур</a>
        </SlideDescription>
      </Content>
    </Wrapper>
);


const Wrapper = styled.div`
  height: 488px;
  background: url(${label}) center center no-repeat, url(${bg}) center center no-repeat;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Content = styled.div`
  max-width: 55rem;
  text-align: center;
  position: relative;
  top: 85px;
`;

export default ToursHead;
