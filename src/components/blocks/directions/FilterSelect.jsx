import React from 'react';
import styled from 'styled-components';
import Select from 'react-select';

const placeholderColor = '#9e9e9e';
const arrowBg = '#cecece';

export const FilterSelect = styled(Select)`
  width: 100%;
  z-index: 3; 

  .Select-control {
    border-radius: 0;
    height: 3rem;
    width: 100%;
  }

  .Select-placeholder {
    color: ${placeholderColor};
    font-size: 1.1rem;
    font-weight: bold;
    text-align: center;
    padding-top: 0.25rem;
  }

  .Select-arrow-zone {
    background: ${arrowBg};
    width: 3rem;
  }

  .Select-arrow {
    border-color: #fff rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
  }

  .Select-option {
    font-weight: bold;
    text-align: center;    
  }
`;

export default FilterSelect;
