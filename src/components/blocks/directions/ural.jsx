import React from 'react';
import Slide from '../Slide';
import { GreenButton } from '../../../Buttons';

import title from './ural.png';
import bg from './ural_bg.png';

const slide = (
  <Slide bgUrl={bg}>
    <div style={{ marginTop: '310px', width: '974px', textAlign: 'center' }}>

      <div className="title">ПОХОД НА ПРИПОЛЯРНЫЙ УРАЛ</div>
      <div><img src={title} width="462" height="102" /></div>
      <div className="description">
        Урал - одни из самых древних гор на планете, это место поистине одно из самых красивых в России.
        Восхождение на высшую точку Урала, кристальные озёра, ледники, заснеженные вершины, дикие северные олени - 
        всё это делает данное место уникальным, в которое вам захочется вернуться вновь.
    </div>

      <GreenButton style={{ marginTop: '4.5rem' }}>ЗАПИСАТЬСЯ</GreenButton>
    </div>
  </Slide>
)

export default slide;
