import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import OfferCard from './OfferCard';
import Filter from './Filter';
import BlockTitle from '../../typography/BlockTitle';
import { BlueButton } from '../../Buttons';

import bg from './bg.png';
import offer1 from './offer1.png';
import offer2 from './offer2.png';

const Directions = ({ className, rows = 1 }) => (

  <div className={className}>
    <Wrapper>

      <Title center="xs" middle="xs">
        <Col xs={12}>
          <BlockTitle>Наши направления</BlockTitle>
        </Col>
      </Title>

      <Filter />

      {[...Array(rows)].map((_, i) => (


        <Row key={i}>
          <Col xs={3}>
            <OfferCard
              imgUrl={offer1}
              price="30 000 руб."
              difficulty="средняя"
              days="16"
              type="без рюкзака"
              km="100"
            />
          </Col>

          <Col xs={3}>
            <OfferCard imgUrl={offer2}
              price="30 000 руб."
              difficulty="средняя"
              days="16"
              type="без рюкзака"
              km="100"
            />
          </Col>

          <Col xs={3}>
            <OfferCard
              imgUrl={offer2}
              price="30 000 руб."
              difficulty="средняя"
              days="16"
              type="без рюкзака"
              km="100"
            />
          </Col>

          <Col xs={3}>
            <OfferCard
              imgUrl={offer2}
              price="30 000 руб."
              difficulty="средняя"
              days="16"
              type="без рюкзака"
              km="100"
            />
          </Col>

        </Row>

      ))}

      <Row center="xs" middle="xs">
        <BlueButton>ПОКАЗАТЬ ВСЕ</BlueButton>
      </Row>

    </Wrapper>
  </div>
);

export default styled(Directions)`
  ${p => !p.noBg && `
    background: url(${bg}) center center;  
  `}
`;

const Wrapper = styled(Grid)`
  padding: 2rem 0 3rem 0;
  
  & > *:not(:last-child) {
    margin-bottom: 2rem;
  }
`;

const Title = styled(Row)`
`;
