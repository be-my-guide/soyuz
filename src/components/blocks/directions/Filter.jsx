import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'react-styled-flexboxgrid';
import { BlueButton, GreenButton } from '../../Buttons';
import Select from './FilterSelect';


export const Filter = ({ className }) => (

  <Wrapper>
    <Row>
      <FilterCol xs={12}>

        <FilterForm>

          <div>
            <Select
              options={[
                { value: 'Бурундия', label: 'Бурундия' },
                { value: 'Руганда', label: 'Руганда' },
              ]}
              rtl
              placeholder="СТРАНА"
            />
          </div>

          <div>
            <Select
              options={[
                { value: 'Бурундия', label: 'Бурундия' },
                { value: 'Руганда', label: 'Руганда' },
              ]}
              rtl
              placeholder="ВРЕМЯ ГОДА"
            />
          </div>

          <div>
            <Select
              options={[
                { value: 'Бурундия', label: 'Бурундия' },
                { value: 'Руганда', label: 'Руганда' },
              ]}
              rtl
              placeholder="ВИД ТУРА"
            />
          </div>

          <div>
            <GreenButton style={{ width: '100%' }}>ПОИСК</GreenButton>
          </div>

        </FilterForm>

      </FilterCol>


    </Row>

  </Wrapper>

);

export default Filter;

const Wrapper = styled.div`
  background-color: ${p => p.theme.color.blue};
`;

const FilterCol = styled(Col)`
  padding: 1rem;
`;

const FilterForm = styled.div`
  display: flex;
  flex-direction: row;
  z-index: 3;

  & > div {
    box-sizing: border-box;
    flex: 0 0 25%;
    padding: 0 1rem;
    display: flex;
    align-items: stretch;
    justify-content: stretch;
  }
`;
