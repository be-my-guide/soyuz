import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';
import BlogTags from './BlogTags';
import { BlogPostBody, BlogPostDate, BlogPostTitle } from '../../typography';
import { VkBlueIconLink, InstaBlueIconLink } from '../../icons';

export const BlogPost = props => (
  <Root>

    <main>
      {props.title && (
        <BlogPostTitle>{props.title}</BlogPostTitle>
      )}

      {props.date && (
        <BlogPostDate>{props.date}</BlogPostDate>
      )}

      {props.content && (
        <BlogPostBody>{props.content}</BlogPostBody>
      )}
    </main>




    <footer>
      <BlogTags tags={props.tags} />
      <div className="blogpost__social">
        {props.vk && <InstaBlueIconLink href={props.vk} />}
        {props.insta && <VkBlueIconLink href={props.insta} />}
      </div>
    </footer>
  </Root>
);

BlogPost.propTypes = {
  className: P.string,

  // is it a digest (preview) or a full article
  digest: P.bool,

  title: P.string,
  date: P.string,
  content: P.node,
  tags: P.arrayOf(P.string),
  insta: P.string,
  vk: P.string
}

export default BlogPost;

const Root = styled.div`
  background: #fff;
  box-shadow: 0 0 13px rgba(0, 0, 0, 0.12);

  &:not(:last-child) {
    margin-bottom: 2rem;
  }

  main {
    padding: 2rem 2rem 0 2rem;
  }

  ${BlogPostBody} {
    padding-top: 1rem;
  }

  footer {
    background: #fdfdfd;
    display:flex;
    justify-content: space-between;
    mon-height: 4rem;
    padding: 1rem 2rem;
  }

  .blogpost__social {
    & > *:not(:last-child) {
      margin-right: 0.25rem;
    }
  }

`;
