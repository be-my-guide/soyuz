import React from 'react';
import styled from 'styled-components';
import { MediumBlueButton } from '../../Buttons';

const BlogLink = styled(MediumBlueButton).attrs({
  simple: p => !p.active
})`
  font-weight: 600;
  display: block;
`;

const BlogMenu = ({ className }) => (

  <div className={className}>
    <BlogLink active>Путешествия</BlogLink>
    <BlogLink>Live</BlogLink>
    <BlogLink>Советы</BlogLink>
    <BlogLink>Снаряжение</BlogLink>    
  </div>

);

export default styled(BlogMenu)`
  display: flex;
  flex-direction: column;
  justify-content: stretch;

  ${BlogLink}:not(:last-child) {
    margin-bottom: 0.5rem;
  }

`;
