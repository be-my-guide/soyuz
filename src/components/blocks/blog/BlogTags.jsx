import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';
import { MediumBlueButton } from '../../Buttons';

const Tag = MediumBlueButton.extend`
   &:not(:last-child) {
    margin-right: 0.25rem;
  }
`;

export const Tags = props => (
  <div>
    {props.tags.map(tag => (
      <Tag>{tag}</Tag>
    ))}

  </div>
)

Tags.propTypes = {
  tags: P.arrayOf(P.string)
};

Tags.defaultProps = {
  tags: []
};

export default Tags;
