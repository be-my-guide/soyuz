import React from 'react';
import styled from 'styled-components';
import { SlideDescription } from '../../typography';

import bg from './bg.png';
import label from './label.png';

const BlogHead = ({ className }) => (
    <div className={className}>
      <Content>
        <SlideDescription>
          Путешествия стали неотьемлемой частью моей жизни
          об этом я и делюсь с вами в своём блоге.
        </SlideDescription>
      </Content>
    </div>
);


export default styled(BlogHead)`
  height: 488px;
  background: url(${label}) center center no-repeat, url(${bg}) center center no-repeat;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Content = styled.div`
  max-width: 30rem;
  text-align: center;
  position: relative;
  top: 85px;
`;


