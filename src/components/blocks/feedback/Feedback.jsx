import React from 'react';
import styled from 'styled-components';
import { Carousel } from 'react-responsive-carousel';
import { Grid, Row, Col } from 'react-styled-flexboxgrid';
import { transparentize } from 'polished';
import faker from 'faker';
import Controls from '../../CarouselControls';
import Overlay from '../../Overlay';
// import Legend from '../../CarouselLegend';
import BlockTitle, { BlockSubtitle } from '../../typography/BlockTitle';
import { BlueButton } from '../../Buttons';
import Quote from '../../Quote';
import { absolutelyStretched, media } from '../../utils';

import CarouselSlide from './Slide';

import img1 from './bg1-dark.png';
import author1 from './author1.png';


export const Feedback = () => (

  <Wrapper>
    <Controls/>

    <Header>
      <Row>
        <Col xs={12}>
          <BlockTitleStyled color="white" center>ОТЗЫВЫ</BlockTitleStyled>
          <BlockSubtitle color="white" center>Яркие отзывы наших новоиспеченых туристов</BlockSubtitle>
        </Col>
      </Row>
    </Header>

    <Carousel
      autoPlay={false}
      showArrows={false}
      showStatus={false}
      showThumbs={false}
    >
      <CarouselSlide
        bgUrl={img1}
        content={(
          <Grid>
            <Row>
              <Col xsOffset={1} xs={10}>

                <QuoteStyled
                  imgUrl={author1}
                  name="Ирина Овдина"
                  occupation="предприниматель, 27 лет."
                  location="Камчатка"
                  text={<React.Fragment>
                    <div>{faker.lorem.paragraph()}</div>
                    <div>{faker.lorem.paragraph()}</div>
                  </React.Fragment>}
                  vk="#"
                  insta="#"
                />
              </Col>
            </Row>

            <Row center="xs">
              <Col xs={12}><BlueButton>ХОЧУ С ВАМИ</BlueButton></Col>
            </Row>
          </Grid>        
        )}
      />

      <CarouselSlide
        bgUrl={img1}
        content={(
          <Grid>
            <Row>
              <Col xsOffset={1} xs={10}>

                <QuoteStyled
                  imgUrl={author1}
                  name="Ирина Овдина"
                  occupation="предприниматель, 27 лет."
                  location="Камчатка"
                  text={<React.Fragment>
                    <p>{faker.lorem.paragraph()}</p>
                    <p>{faker.lorem.paragraph()}</p>
                  </React.Fragment>}
                  vk="#"
                  insta="#"
                />
              </Col>
            </Row>

            <Row center="xs">
              <Col xs={12}><BlueButton>ХОЧУ С ВАМИ</BlueButton></Col>
            </Row>
          </Grid>        
        )}
      />
    </Carousel>
   

  </Wrapper>
);

export default Feedback;

const Wrapper = styled.div`
  position: relative;

  .control-dots {

    ${media.md`
      bottom: 4rem !important;
    `}

    z-index: 2; // carousel dots above black overlay
  }
`;

const Header = styled.div`
  position: absolute;
  top: 3rem;
  right: 0;
  left: 0;
  z-index: 2;  
`;

const BlockTitleStyled = styled(BlockTitle)`
  margin-bottom: 0.25rem;
`;

const QuoteStyled = styled(Quote)`
  background: white;
  margin-bottom: 3rem;
`;
