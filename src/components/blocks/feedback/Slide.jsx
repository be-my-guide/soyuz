import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';
import { Grid, Row, Col } from 'react-styled-flexboxgrid';
import { media } from '../../utils';


export const Slide = ({ bgUrl, content }) => (
  <Wrapper src={bgUrl}>
    <Legend>
      {content}
    </Legend>
  </Wrapper>
);

Slide.propTypes = {
  bgUrl: P.string.isRequired,
  content: P.node
}

export default Slide;

const Wrapper = styled.div`
  background: url(${p => p.src}) center center no-repeat;
  background-size: cover;
  min-height: 824px;
  padding: 12rem 0 3rem 0;

  ${media.md`

  `};  
`;

const Legend = styled.div`
text-align: left !important;
`;


