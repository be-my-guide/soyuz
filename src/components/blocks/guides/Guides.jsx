import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import Guide from './Guide';
import { BlockTitle } from '../../typography/BlockTitle';
import { media } from '../../utils';

import g1 from './guide-photo-1.png';
import g2 from './guide-photo-2.png';
import g3 from './guide-photo-3.png';
import g4 from './guide-photo-4.png';
import g5 from './guide-photo-5.png';

export const Guides = () => (

  <Bg>
    <GridStyled>
      <Row>
        <ColStyled xs={12}>
          <Title>
            <BlockTitle>НАШИ ГИДЫ</BlockTitle>
          </Title>

          <Faces center="xs">
            <GuideCol>
              <Guide imgUrl={g1} name="Пётр Мамонов" comment="27 походов"/>
            </GuideCol>

            <GuideCol>
              <Guide imgUrl={g2} name="Вероника Панина" comment="105 походов"/>
            </GuideCol>

            <GuideCol>
              <Guide imgUrl={g3} name="Игорь Жук" comment="54 похода"/>
            </GuideCol>

            <GuideCol>
              <Guide imgUrl={g4} name="Вероника Панина" comment="12 походов"/>
            </GuideCol>

            <GuideCol>
              <Guide imgUrl={g5} name="Пётр Мамонов" comment="27 походов"/>
            </GuideCol>
          </Faces>
        </ColStyled>
      </Row>
    </GridStyled>

  </Bg>


);

export default Guides;

const Bg = styled.div`
  padding: 4.5rem 0;
`;

const GridStyled = styled(Grid)`
`;

const ColStyled = styled(Col)`
  border: 0.25rem solid ${p => p.theme.color.lightgrey1};
`;

const GuideCol = styled(Col).attrs({
  xs: 3,
  md: 2
})``;

const Title = styled.div`
  text-align: center;
  
  & > * {
    background-color: ${p => p.theme.color.lightgrey};
    bottom: 0.78rem;
    padding: 0 1rem;
    position: relative;
  }
`;

const Faces = styled(Row)`

  margin: 1rem 0;
  
  &:not(:last-child) {
    padding-right: 1rem;
  }

  ${media.md`
    margin: 2rem 0;
    
    &:not(:last-child) {
      padding-right: 2rem;
    }
  `}

`;
