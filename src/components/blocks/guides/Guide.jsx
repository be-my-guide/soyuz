import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';

export const Guide = props => (

  <Wrapper>
    <Img src={props.imgUrl} />
    <Name>{props.name}</Name>
    <Desc>{props.comment}</Desc>
  </Wrapper>


);

Guide.propTypes = {
  imgUrl: P.string.isRequired,
  name: P.string.isRequired,
  comment: P.string.isRequired
};

export default Guide;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Img = styled.div`
  position: relative;
  
  &:before {
    padding-top: 100%;
    background: url(${p => p.src}) no-repeat;
    background-size: cover;
    border-radius: 50%;
    display: block;
    content: ' ';
  }
  
`;

const Name = styled.div`
  font-weight: bold;
  text-align: center;
  margin-top: 1rem;
`;

const Desc = styled.div`
  font-style: italic;
  text-align: center;
`;
