import React from 'react';
import styled from 'styled-components';

import bg from './bg';
import label from './label';

export const IndividualToursHead = styled.div`
  height: 488px;
  background: url(${bg}) center center no-repeat, url(${label}) center center no-repeat;
`;

export default IndividualToursHead;
