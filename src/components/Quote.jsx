import React from 'react';
import styled from 'styled-components';
import P from 'prop-types';
import { InstaBlueIconLink, VkBlueIconLink } from './icons';

export const Quote = props => (

  <Wrapper fitted={props.fitted} className={props.className}>
    <ImgCol>
      <img src={props.imgUrl} style={{ borderRadius: '50%'}} />
    </ImgCol>

    <Main>
      <Name>{props.name}</Name>

      <Occupation>{props.occupation}</Occupation>

      {props.location && (
        <Location>{props.location}</Location>
      )}

      <Text>{props.text}</Text>
      
      <Social>
        {props.vk && (
          <InstaBlueIconLink href={props.vk} />
        )}

        {props.insta && (
          <VkBlueIconLink href={props.insta} />
        )}
      </Social>

      {props.extra}

    </Main>
  </Wrapper>

);

Quote.propTypes = {
  // no padding inside
  fitted: P.bool,

  imgUrl: P.string.isRequired,
  name: P.string.isRequired,
  occupation: P.string.isRequired,
  location: P.string,
  text: P.node.isRequired,
  vk: P.string,
  insta: P.string,
  extra: P.node
};

export default Quote;

const Wrapper = styled.div`
  display: flex;
  ${p => !p.fitted && `
    padding: 3rem;  
  `}
`;

const ImgCol = styled.div``;

const Main = styled.div`
  flex: 1;
  padding-left: 3rem !important;
  display: flex;
  flex-direction: column;
`;

const Name = styled.div`
  font-weight: bold;
  font-size: 1.25rem;
`;

const Occupation = styled.div`
  font-weight: bold;
  font-size: 1.1rem;
  color: ${p => p.theme.color.greytext};
  padding-bottom: 1.25rem;  
`;

const Location = Name.extend`

`;

const Text = styled.div`
  font-size: 0.9rem;
  font-style: italic;
  font-weight: 400;
  
  & > *:not(:last-child) {
    padding-bottom: 0.5rem;
  }

  &:not(:last-child) {
    margin-bottom: 1rem;
  }
`;

const Social = styled.div`
  align-items: flex-end;
  display: flex;
  flex: 1;
  
  & > * {
    margin-right: 0.25rem;
  }

  &:not(:last-child) {
    margin-bottom: 1rem;
  }  
`;
