import React from 'react';
import styled, { css } from 'styled-components';

const borders = css`
  border-radius: 0;
  border: 1px solid black;
  padding: 0.75rem;
`;

const font = css`
  font-size: 1.1rem;
  font-family: ${p => p.theme.fontFamily};
`;

export const InputText = styled.input.attrs({
  type: 'text'
})`
  ${borders};
  ${font};
`;

export const TextArea = styled.textarea`

  resize: vertical;

  ${borders};
  ${font};
`;
