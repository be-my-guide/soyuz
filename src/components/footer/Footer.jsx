import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import MailIcon from 'react-icons/lib/fa/envelope-o';
import PhoneIcon from 'react-icons/lib/fa/phone';
import MapIcon from 'react-icons/lib/fa/map-marker';
import FbIcon from 'react-icons/lib/fa/facebook';
import VkIcon from 'react-icons/lib/fa/vk';
import Circle from '../CircleAroundIcon';

const circleProps = {
  color: () => '#367d9d',
  backgroundColor: () => 'white',
  size: '1.8rem',
};

const fb = <Circle {...circleProps}><FbIcon/></Circle>;
const vk = <Circle {...circleProps}><VkIcon/></Circle>;

const Footer = ({ className }) => (

  <div className={className}>
    <Grid>
      <Row>

        <Col xs={6} md={4} mdOffset={1}>
          <Label>КОНТАКТЫ</Label>

          <Req><MailIcon/> <a href="#">souz18@mail.ru</a></Req>
          <Req><PhoneIcon/> +7&nbsp;(922)&nbsp;505&ndash;00&ndash;91</Req>
          <Req><MapIcon/> Москва, дом 210 оф. 9</Req>
        
        </Col>

        <Col xs={6} md={3} mdOffset={4}>
          <Label>МЫ В СОЦ. СЕТЯХ</Label>

          <Req><a href="#">{fb}</a> <a href="#">FACEBOOK</a></Req>
          <Req><a href="#">{vk}</a> <a href="#">Москва, дом 210 оф. 9</a></Req>          
        </Col>        

      </Row>
    </Grid>
  </div>

);

export default styled(Footer)`
  color: white;
  font-size: 0.9rem;
  padding: 6rem 0 0 0;

  a {
    color: white;
  }
`;

const Label = styled.div`
  font-weight: bold;
  font-size: 1.1rem;
  padding-bottom: 2rem;
`;

const Req = styled.div`
  padding-bottom: 1rem;
`;
