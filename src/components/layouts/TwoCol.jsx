import React from 'react';
import P from 'prop-types';
import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';

const Layout = ({ className, sidebar, children }) => (
  <div className={className}>
    <Grid>
      <Row>
        <Col xs={12} md={3} lg={2}>{sidebar}</Col>
        <Col xs={12} md={9} lg={10}>{children}</Col>
      </Row>
    </Grid>
  </div>
);

Layout.propTypes = {
  sidebar: P.node
};

export default styled(Layout)`

`;
