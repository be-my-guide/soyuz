import React from 'react';
import styled from 'styled-components';
import MainMenu from '../MainMenu';
import Footer from '../footer/Footer';

import bg from './footer-bg';

const Layout1 = ({ children, className }) => (

  <div className={className}>
    <MainMenu />
    {children}
    <Footer />
  </div>

);

export default styled(Layout1)`
  background: url(${bg}) center bottom no-repeat, ${p => p.theme.color.background};
  display: flex;
  flex: 1;
  flex-direction: column;

  ${Footer} {
    margin-bottom: 18.75rem;
    margin-top: auto;
  }
`;
