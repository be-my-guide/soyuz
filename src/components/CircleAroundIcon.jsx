import React from 'react';
import styled from 'styled-components';
import { darken } from 'polished';

export const CircleAroundIcon = styled.span`
  display: inline-flex;
  color: ${p => p.color ? p.color(p) : 'white'};
  background-color: ${p => p.backgroundColor ? p.backgroundColor(p) : 'black'};  
  border-radius: 50%;
  width: ${p => p.size || '1.5rem'};
  height: ${p => p.size || '1.5rem'};
  align-items: center;
  justify-content: center;

  transition: all ${p => p.theme.transition.default};

  &:hover, &:active, &:focus {
    background-color: ${p => darken(0.2, p.backgroundColor ? p.backgroundColor(p) : 'black')};
  }
  
`;

export default CircleAroundIcon;
