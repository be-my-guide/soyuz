export const fontsize = {
  xs: '0.6rem',
  sm: '0.75rem',
  mds: '0.9rem',
  md: '1rem',
  lg: '1.1rem',
  xl: '1.25rem'
};

export const fontFamily = "Open Sans, Helvetica Neue, sans-serif";

export const color = {
  blue: 'rgb(0, 89, 215)',
  darkblue: '#000531',
  green: 'rgb(0, 255, 25)',
  grey: 'grey',
  lightgrey1: '#d6d6d6',
  lightgrey: '#f4f4f4',
  background: '#f4f4f4',
  greytext: '#858585'
};

const flexboxgrid = {
  gridSize: 12, // rem
  gutterWidth: 1, // rem
  outerMargin: 2, // rem
  mediaQuery: 'only screen',
  container: {
    sm: 46, // rem
    md: 61, // rem
    lg: 76  // rem
  },
  breakpoints: {
    xs: 0,  // em
    sm: 48, // em
    md: 64, // em
    lg: 75  // em
  }
};

const layout = {
  mainmenuHeight: '7.5rem'
};

const transition = {
  default: '350ms ease-out'
};

export default {
  color,
  fontsize,
  fontFamily,
  flexboxgrid,
  layout,
  transition
};
