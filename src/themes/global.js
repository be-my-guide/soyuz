import { injectGlobal } from 'styled-components';
import reset from 'styled-reset';
import { normalize } from './normalize';
import { carousel } from './carousel';

import 'react-select/dist/react-select.css';

export default () => injectGlobal`
  ${normalize}
  ${carousel}
  
  input[type=text], textarea {
    box-sizing: border-box;
  }
  
  @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600');

  #root {
    display: flex;
    flex-direction: column;
    width: 100vw;
    min-width: 768px;
    max-width: 100vw;
    min-height: 100vh;
    font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
  
  html {
    font-size: 16px;
    font-family: ${p => p.theme.fontFamily};

    * {
      font-family: ${p => p.theme.fontFamily};      
    } 
  }

  body {
    overflow-x: hidden;
    max-width: 100vw;
  }

`;
