import React from 'react';
import styled from 'styled-components';
import { media } from '../components/utils';


export const TestMediaQueries = styled.div`
  
  content: 'xs';
  display: block;
  height: 2rem;
  background-color: green;

  ${media.sm`
    content: 'sm';
    background-color: orange;
  `}
  
  ${media.md`
    content: 'md';
    background-color: pink;
  `}

  ${media.lg`
    content: 'lg';
    background-color: red;
  `}


`;

export default TestMediaQueries;
