import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch } from 'react-router';
import { ThemeProvider } from 'styled-components';

import theme from './themes/default';
import globals from './themes/global';

import Frontpage from './pages/frontpage';
import IndividualToursPage from './pages/individual_tours';
import ToursPage from './pages/tours';
import Blog from './pages/blog';

globals();

const App = () => (

  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <Switch>
        <Route path="/tours"><ToursPage /></Route>
        <Route path="/individual_tours"><IndividualToursPage /></Route>
        <Route path="/blog"><Blog /></Route>
        <Route path="/" strict><Frontpage /></Route>        
      </Switch>
    </BrowserRouter>
  </ThemeProvider>

);

window.onload = () => {
  render(<App />, document.getElementById('root'));
};

