import React from 'react';
import styled from 'styled-components';
import MainMenu from '../components/MainMenu';
import Intro from '../components/blocks/intro/Intro';
import Directions from '../components/blocks/directions/Directions';
import Feedback from '../components/blocks/feedback/Feedback';
import Application from '../components/blocks/application/Application';
import Guides from '../components/blocks/guides/Guides';
import BlogAuthor from '../components/blocks/blog_author/BlogAuthor';

import Layout from '../components/layouts/Layout1';

export const Frontpage = () => (

  <Layout>
    <IntroStyled />
    <Directions />
    <Feedback />
    <Application withBg={true} />
    <Guides />
    <BlogAuthor />
  </Layout>

);

export default Frontpage;

const IntroStyled = styled(Intro)`
  
`;

