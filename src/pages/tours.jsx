import React from 'react';
import styled from 'styled-components';
import Head from '../components/blocks/directions/ToursHead.jsx'
import Layout from '../components/layouts/Layout1';
import Directions from '../components/blocks/directions/Directions';

const ToursPage = ({ className }) => (
  <Layout className={className}>
    <Head />
    <Directions rows={2} noBg={true} />    
  </Layout>
);

export default styled(ToursPage)`
  ${Directions} {
    margin-bottom: 8rem;
  }
`;

