import React from 'react';
import Application from '../components/blocks/application/Application';
import Head from '../components/blocks/individual_tours/IndividualToursHead.jsx'
import Layout from '../components/layouts/Layout1';

export const IndividualToursPage = () => (

  <Layout>
    <Head />
    <Application transpBg={true} rucksack={false} />
  </Layout>

);

export default IndividualToursPage;

