import React from 'react';
import styled from 'styled-components';
import Head from '../components/blocks/blog/BlogHead'
import Layout from '../components/layouts/Layout1';
import TwoCol from '../components/layouts/TwoCol';
import BlogMenu from '../components/blocks/blog/BlogMenu';
import BlogPost from '../components/blocks/blog/BlogPost';

const Blog = ({ className }) => (
  <Layout className={className}>
    <Head />

    <TwoCol sidebar={<BlogMenu />}>
      <BlogPost
        digest={true}
        title="АЛТАЙ БЕЛУХА"
        date="07.02.2016"
        tags={['Путешествия', 'Советы']}
        insta="#"
        vk="#"
        content={(
          <React.Fragment>
            <img
              src="http://zlato-altay.ru/images/dynamicslideshow/slides/image3.jpg"
              width="100%"
            />
            <p>
              Приглашаем принять участие в увлекательном путешествии по красивейшему маршруту России
              к подножию Белухи на Алтай. Поход для тех, кто знает что такое рюкзак, и уже имеет хотя бы 
              самый небольшой опыт и имеет среднюю физическую подготовку.
            </p>
          </React.Fragment>

        )}
      />

      <BlogPost
        digest={true}
        title="ГОРНОАЛТАЙСК, РОССИЯ"
        date="07.02.2016"
        tags={['Live']}
        insta="#"
        vk="#"
        content={(
          <React.Fragment>
            <img
              src="http://zlato-altay.ru/images/dynamicslideshow/slides/image3.jpg"
              width="100%"
            />
            <p>
              Приглашаем принять участие в увлекательном путешествии по красивейшему маршруту России
              к подножию Белухи на Алтай. Поход для тех, кто знает что такое рюкзак, и уже имеет хотя бы 
              самый небольшой опыт и имеет среднюю физическую подготовку.
            </p>
          </React.Fragment>

        )}
      />


    </TwoCol>

  </Layout>
);

export default styled(Blog)`

  ${TwoCol} {
    min-height: 30rem;
    margin-top: 3rem;
  }

`;

